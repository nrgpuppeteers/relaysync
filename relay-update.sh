#! /bin/bash

source /root/relay-data.sh
source /opt/ozmt/zfs-tools-init.sh

pools=$(pools)

##
## Create .netrc
##

[ -f ~/.netrc ] && rm -f ~/.netrc
touch ~/.netrc
chmod 600 ~/.netrc
echo "machine $owncloud" > ~/.netrc
echo "login $oc_user" >> ~/.netrc
echo "password $oc_password" >> ~/.netrc

##
## Collect log data
##

mkdir -p /owncloud/${HOSTNAME}/log/

rsync -a /var/log/ /owncloud/${HOSTNAME}/log/

##
## Collect stats
##


mkdir -p /owncloud/${HOSTNAME}/stats
/opt/puppetlabs/bin/facter -j > /owncloud/${HOSTNAME}/stats/facter.json
zfs list -t all > /owncloud/${HOSTNAME}/stats/zfs-list.txt
uptime > /owncloud/${HOSTNAME}/stats/uptime.txt



##
## Sync Owncloud
##

owncloudcmd --non-interactive --silent -n /owncloud ownclouds://${owncloud}/owncloud/remote.php/webdav  1>/var/log/owncloud_sync.log 2>/var/log/owncloud_sync_error.log
